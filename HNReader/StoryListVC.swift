//
//  StoryListVC.swift
//  HNReader
//
//  Created by Javi Manzano on 19/03/2015.
//  Copyright (c) 2015 Javi Manzano. All rights reserved.
//

import UIKit

class StoryListVC: UITableViewController {

    // MARK: - VC properties
    let reuseIdentifier = "StoryCell"
    
    var storiesController = StoriesController()
    
    var lastUpdated: Date?
    
    var storyList: [Story] = []
    
    // MARK: - Outlets

    @IBOutlet weak var moreStoriesButton: UIBarButtonItem!
    
    // MARK: - Override functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set up the tableview
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.estimatedRowHeight = 50
        tableView.rowHeight = UITableViewAutomaticDimension
        
        // Define a notification to fetch the stories when the app enters in foreground.
        NotificationCenter.default.addObserver(
            self,
            selector:#selector(StoryListVC.autoReloadStories),
            name: NSNotification.Name.UIApplicationWillEnterForeground,
            object: nil)
        
        // Fetch the stories initially
        getTopStories()
        
        // Pull to refresh
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(StoryListVC.getTopStories), for: UIControlEvents.valueChanged)
        if let refreshControl = refreshControl {
            tableView.addSubview(refreshControl)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowStoryDetail" {
            if let indexPath = tableView.indexPathForSelectedRow as IndexPath! {
                let destinationVC = segue.destination as! StoryWebViewVC
                
                destinationVC.story = storyList[(indexPath as NSIndexPath).row]
                destinationVC.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                destinationVC.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return storyList.count;
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Get the table cell
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier) as! StoryCell
        
        // Set the cell content.
        cell.setContent(cellIndex: (indexPath as NSIndexPath).row + 1, story: storyList[(indexPath as NSIndexPath).row])
        
        return cell
    }
    
    // MARK: - IBActions
    @IBAction func moreStories (_ sender: AnyObject) {
        getNextTopStories()
    }
    
    // MARK: - Functions
    
    /*
     Fetch the stories automatically when
    */
    func autoReloadStories () {
        if let lastUpdated = lastUpdated {
            let now = Date()
            let difference = (Calendar.current as NSCalendar).components(
                NSCalendar.Unit.second,
                from: lastUpdated,
                to: now,
                options: [])
            
            // Don't update more than once per two minutes.
            if difference.second! < 120 {
                return
            }
        }
        
        // Fetch the files and directories in the current directory.
        getTopStories()
    }
    
    func onStoriesLoadSuccess(_ stories: [Story]) {
        refreshControl?.endRefreshing()
        
        storyList = stories
        moreStoriesButton.isEnabled = true
        
        tableView.reloadData()
    }
    
    func onMoreStoriesLoadSuccess (_ stories: [Story]) {
        refreshControl?.endRefreshing()
        
        storyList += stories
        moreStoriesButton.isEnabled = false
        
        tableView.reloadData()
    }
    
    func onStoriesLoadError() {
        refreshControl?.endRefreshing()
        
        // Show error message
        let alertController = UIAlertController(
            title: "Ooops",
            message: "There was an error fetching the top stories. Please, try again later.",
            preferredStyle: UIAlertControllerStyle.alert)
        
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
        
        present(alertController, animated: true, completion: nil)
    }
    
    func getTopStories () {
        refreshControl?.beginRefreshing()
        storiesController.getTopStories(onStoriesLoadSuccess, onTaskError: onStoriesLoadError)
        lastUpdated = Date()
    }
    
    func getNextTopStories () {
        moreStoriesButton.isEnabled = false
        refreshControl?.beginRefreshing()
        storiesController.getNextThirtyStories(onMoreStoriesLoadSuccess, onTaskError: onStoriesLoadError)
    }
}
