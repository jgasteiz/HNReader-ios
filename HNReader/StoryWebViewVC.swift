//
//  .swift
//  HNReader
//
//  Created by Javi Manzano on 25/03/2015.
//  Copyright (c) 2015 Javi Manzano. All rights reserved.
//

import UIKit

class StoryWebViewVC: UIViewController {
    
    var storiesController = StoriesController()
    
    var story: Story?
    
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var bottomToolbar: UIToolbar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        automaticallyAdjustsScrollViewInsets = false

        if let story = story {
            navigationItem.title = story.getTitle()
            
            if story.getURL() != "" {
                if let storyNSURL = URL(string: story.getURL()) {
                    let request = URLRequest(url: storyNSURL)
                    webView.scalesPageToFit = true
                    webView.loadRequest(request)
                }
            } else {
                // Load story content
                fetchStoryContent()
                bottomToolbar.isHidden = true
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowStoryComments" {
            let destinationVC = segue.destination as! StoryCommentsVC
            destinationVC.story = story
        }
    }

    @IBAction func shareStory(_ sender: UIBarButtonItem) {
        guard let story = story else {
            return
        }
        
        let objectsToShare = [URL(string: story.getURL())!]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        activityVC.popoverPresentationController?.barButtonItem = sender
        present(activityVC, animated: true, completion: nil)
    }
    
    @IBAction func openSafari(_ sender: AnyObject) {
        guard let story = story else {
            return
        }
        
        UIApplication.shared.openURL(URL(string: story.getURL())!)
    }
    
    
    @IBAction func viewComments(_ sender: AnyObject) {
        performSegue(withIdentifier: "showComments", sender: self)
    }
    
    func fetchStoryContent() {
        guard let story = story else {
            return
        }
        
        storiesController.getStoryDetail(
            story.getId(),
            onTaskDone: onGetStorySuccess,
            onTaskError: onGetPostsError
        )
    }
    
    func onGetStorySuccess(_ story: Story) {
        self.story = story
        if let storyHTML = story.getHTML() as String? {
            webView.loadHTMLString(storyHTML, baseURL: nil)
        }
    }
    
    func onGetPostsError() {
        // Show error message
        let alertController = UIAlertController(title: "Ooops", message:
            "There was an error fetching the top stories. Please, try again later.", preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
        
        present(alertController, animated: true, completion: nil)
    }
}
