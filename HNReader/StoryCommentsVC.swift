//
//  StoryCommentsVC.swift
//  HNReader
//
//  Created by Javi Manzano on 25/03/2015.
//  Copyright (c) 2015 Javi Manzano. All rights reserved.
//

import UIKit

class StoryCommentsVC: UITableViewController {
    
    let reuseIdentifier = "CommentCell"
    
    var storiesController = StoriesController()
    
    var story: Story?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 50
        tableView.rowHeight = UITableViewAutomaticDimension
        
        if let story = story {
            navigationItem.title = story.getTitle()
        }
        
        fetchComments()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fetchComments() {
        guard let story = story else {
            return
        }
        
        storiesController.getStoryComments(story.getId(), onTaskDone: onGetCommentsSuccess, onTaskError: onGetPostsError)
    }
    
    func onGetCommentsSuccess(_ comments: [Comment]) {
        guard let story = story else {
            return
        }
        
        story.comments = comments
        tableView.reloadData()
    }
    
    func onGetPostsError() {
        // Show error message
        let alertController = UIAlertController(title: "Ooops", message:
            "There was an error fetching the top stories. Please, try again later.", preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
        
        present(alertController, animated: true, completion: nil)
    }
}

extension StoryCommentsVC {
    
    override func tableView(_ tableView: UITableView, indentationLevelForRowAt indexPath: IndexPath) -> Int {
        guard let story = story else {
            return 0
        }
        
        return story.comments[(indexPath as NSIndexPath).row].getLevel()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let story = story else {
            return 0
        }
        
        return story.comments.count;
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Get the table cell
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath)  as! StoryCommentCell
        
        // Set the cell content
        if let story = story {
            cell.setContent(comment: story.comments[(indexPath as NSIndexPath).row])
        }
        
        return cell
    }
}
