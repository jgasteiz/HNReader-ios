//
//  StoryCell.swift
//  HNReader
//
//  Created by Javi Manzano on 13/10/2015.
//  Copyright © 2015 Javi Manzano. All rights reserved.
//

import Foundation
import UIKit

class StoryCell: UITableViewCell {
    
    @IBOutlet weak var index: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var url: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var commentsButton: UIButton!
    
    override var bounds: CGRect {
        didSet {
            contentView.frame = bounds
        }
    }
    
    func setContent (cellIndex: Int, story: Story) {
        // Set the labels text with the story values
        index.text = String(cellIndex)
        title.text = story.getTitle()
        url.text = story.getDisplayURL()
        subtitle.text = "\(story.getPoints()) points"
        
        // If the story has a user, append it to the description label
        if story.hasUser() {
            subtitle.text = "\(subtitle.text!), by \(story.getUser())"
        }
        
        // No comment count until it's possible to view the comments in the app.
        subtitle.text = "\(subtitle.text!), \(story.getTimeAgo()), \(story.getCommentsCount()) comments"
    }
}
