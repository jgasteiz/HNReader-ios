//
//  StoriesController.swift
//  HNReader
//
//  Created by Javi Manzano on 19/03/2015.
//  Copyright (c) 2015 Javi Manzano. All rights reserved.
//

import Alamofire
import Foundation

class StoriesController {

    // MARK: - Properties
    
    let topNewsURL: URL = URL(string: "https://api.hackerwebapp.com/news")!
    let nextThirtyURL: URL = URL(string: "https://api.hackerwebapp.com/news2")!
    let baseStoryURL: String = "https://api.hackerwebapp.com/item/"
    
    // MARK: - Stories methods
    
    // Get the first 30 stories
    func getTopStories(_ onTaskDone: @escaping ([Story]) -> Void, onTaskError: @escaping () -> Void) {
        self.fetchStories(topNewsURL, firstThirtyStories: true, onTaskDone: onTaskDone, onTaskError: onTaskError)
    }
    
    // Get the next 30 stories
    func getNextThirtyStories(_ onTaskDone: @escaping ([Story]) -> Void, onTaskError: @escaping () -> Void) {
        self.fetchStories(nextThirtyURL, firstThirtyStories: false, onTaskDone: onTaskDone, onTaskError: onTaskError)
    }
    
    // Get a story
    func getStoryDetail(_ storyId: Int, onTaskDone: @escaping (Story) -> Void, onTaskError: @escaping () -> Void) {
        let storyURL = URL(string: "\(baseStoryURL)\(storyId)") as URL!
        
        self.fetchStory(storyURL!, onTaskDone: onTaskDone, onTaskError: onTaskError)
    }
    
    // Given an api url, a boolean and callbacks, fetch 30 stories from the API.
    func fetchStories(_ storiesURL: URL, firstThirtyStories: Bool, onTaskDone: @escaping ([Story]) -> Void, onTaskError: @escaping () -> Void) -> Void {
        Alamofire
            .request(storiesURL, method: HTTPMethod.get)
            .responseJSON(completionHandler: { response in
                if let storiesArray = response.result.value as? [NSDictionary] {
                    
                    var stories: [Story] = []
                    
                    // Add the fetched stories to the array
                    for storyDict in storiesArray {
                        stories.append(Story(
                            id: storyDict["id"] as? Int,
                            title: storyDict["title"] as? String,
                            user: storyDict["user"] as? String,
                            timeAgo: storyDict["time_ago"] as? String,
                            type: storyDict["type"] as? String,
                            url: storyDict["url"] as? String,
                            points: storyDict["points"] as? Int,
                            commentsCount: storyDict["comments_count"] as? Int,
                            content: storyDict["content"] as? String
                        ))
                    }
                    
                    onTaskDone(stories)
                } else {
                    onTaskError()
                }
            })
    }
    
    // Fetch a single story
    func fetchStory(_ storyURL: URL, onTaskDone: @escaping (_ story: Story) -> Void, onTaskError: @escaping () -> Void) -> Void {
        Alamofire
            .request(storyURL, method: HTTPMethod.get)
            .responseJSON(completionHandler: { response in
                if let storyDict = response.result.value as? NSDictionary {
                    
                    let story: Story = Story(
                        id: storyDict["id"] as? Int,
                        title: storyDict["title"] as? String,
                        user: storyDict["user"] as? String,
                        timeAgo: storyDict["time_ago"] as? String,
                        type: storyDict["type"] as? String,
                        url: storyDict["url"] as? String,
                        points: storyDict["points"] as? Int,
                        commentsCount: storyDict["comments_count"] as? Int,
                        content: storyDict["content"] as? String
                    )
                    
                    onTaskDone(story)
                } else {
                    onTaskError()
                }
            })
    }
    
    // MARK: - Comments methods
    
    // Given a story id, fetch all of its comments.
    func getStoryComments (_ storyId: Int, onTaskDone: @escaping (_ comments: [Comment]) -> Void, onTaskError: @escaping () -> Void) -> Void {
        let storyURL = URL(string: "\(baseStoryURL)\(storyId)") as URL!
        self.fetchComments(storyURL!, onTaskDone: onTaskDone, onTaskError: onTaskError)
    }
    
    func fetchComments(_ storyURL: URL, onTaskDone: @escaping (_ comments: [Comment]) -> Void, onTaskError: @escaping () -> Void) -> Void {
        Alamofire
            .request(storyURL, method: HTTPMethod.get)
            .responseJSON(completionHandler: { response in
                if let responseJson = response.result.value as? NSDictionary {
                    
                    // Get all the comments for the story in a list.
                    let allComments: [Comment] = self.getAllComments(responseJson, comments: [])
                    
                    onTaskDone(allComments)
                } else {
                    onTaskError()
                }
            })
    }
    
    // Given a comment NSDictionary, which could have more comments, return
    // a single level list of all the comments and their children,
    // in hierarchical order.
    func getAllComments (_ comment: NSDictionary, comments: [Comment]) -> [Comment] {
        
        var commentList = comments;
        
        // Loop through the comment dictionary comments
        for commentObject in comment["comments"] as! [NSDictionary] {
            
            // Create a new comment and add it to the general list of comments
            commentList.append(Comment(
                id: commentObject["id"] as? Int,
                level: commentObject["level"] as? Int,
                user: commentObject["user"] as? String,
                timeAgo: commentObject["time_ago"] as? String,
                content: commentObject["content"] as? String
            ))
            
            // If the comment has comments, fetch them and add them to the list.
            if commentObject["comments"] != nil {
                commentList = commentList + self.getAllComments(commentObject, comments: [])
            }
        }
        
        // Return all the comments for the given comment.
        return commentList;
    }
    
}
